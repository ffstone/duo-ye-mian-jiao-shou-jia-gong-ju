const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require("webpack");
const ASSET_PATH = process.env.ASSET_PATH || "./";
module.exports = {
    entry: {
        main: "./src/script/main.js"
    },
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "js/[name].[hash:8].js",
        publicPath: ASSET_PATH
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    {
                        loader: "url-loader",
                        options: {
                            limit: 4096,
                            fallback: {
                                loader: "file-loader",
                                options: {
                                    name: "assests/imgs/[name].[hash:8].[ext]"
                                }
                            }
                        }
                    }
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    {
                        loader: "filt-loader",
                        options: {
                            name: "assests/fonts/[name].[hash:8].[ext]"
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    //     {
                    //     loader: MiniCssExtractPlugin.loader,
                    // },
                    {
                        loader: "style-loader" // 将 JS 字符串生成为 style 节点
                    },
                    {
                        loader: "poscss-loader",
                        options: {
                            sourceMap: true,
                            config: {
                                path: "postcss.config.js"
                            }
                        }
                    },
                    {
                        loader: "css-loader" // 将 CSS 转化成 CommonJS 模块
                    },
                    {
                        loader: "sass-loader" // 将 Sass 编译成 CSS
                    }
                ]
            },
            {
                test: /\.js$/,
                include: "/src/",
                exclude: "/node_modules/",
                use: [
                    {
                        loader: "cache-loader"
                    },
                    {
                        loader: "thread-loader"
                    },
                    {
                        loader: "babel-loader"
                    }
                ]
            },
            {
                enforce: "pre",
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "eslint-loader"
            },
            {
                test: /\.ts$/,
                use: [
                    {
                        loader: "cache-loader"
                    },
                    {
                        loader: "thread-loader"
                    },
                    {
                        loader: "babel-loader"
                    },
                    {
                        loader: "ts-loader"
                    }
                ]
            }
        ]
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                lib: {
                    test: /[\\\/]node_modules[\\\/]/,
                    priority: -10,
                    chunks: "initial",
                    name: "lib"
                }
            }
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            JQuery: "jquery",
            $: "jquery"
        }),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, "./src/index.html"),
            title: "out",
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeAttributeQuotes: true,
                collapseBooleanAttributes: true,
                removeScriptTypeAttributes: true
            },
            chunks: ["lib", "main"]
        }),
        new webpack.HashedModuleIdsPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        // 该插件帮助我们安心地使用环境变量
        new webpack.DefinePlugin({
            "process.env.ASSET_PATH": JSON.stringify(ASSET_PATH)
        })
    ]
};
